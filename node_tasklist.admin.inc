<?php

/**
 * @file
 * Configuration page for node_tasklist
 */

/**
 * Implementation of settings page.
 */
function node_tasklist_admin() {
  $form = array();

  $form['node_tasklist_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Enabled types'),
    '#default_value' => variable_get('node_tasklist_types', array(NULL => NULL)),
    '#options' => array_map('check_plain', node_get_types('names')),
    '#description' => t('After choosing which node types you would like to enable save this page and you will get options for configuring each type.'),
  );

  if (variable_get('node_tasklist_types', NULL)) {
    foreach (node_tasklist_get_enabled_types() as $type => $value) {

      if ($type === $value) {

        if (variable_get('node_tasklist_' . $type . '_sort', NULL) === NULL) {
          drupal_set_message(t('Please configure settings for the %type type and save your changes.', array('%type' => $type)));
        }
        $form[$type] = array(
          '#type' => 'fieldset',
          '#title' => check_plain(node_get_types('name', $type)),
          '#collapsed' => FALSE,
          '#collapsible' => TRUE,
          '#description' => t('Creates a block called "Tasklist: %typename" and a page located at /tl/%type.', array('%typename' => check_plain(node_get_types('name', $type)), '%type' => $type)),
        );
        $form[$type]['node_tasklist_' . $type . '_sort'] = array(
          '#type' => 'radios',
          '#title' => t('Sort method'),
          '#default_value' => variable_get('node_tasklist_' . $type . '_sort', '0'),
          '#options' => array('0' => t('Weekly (use node containing match for YYYY-WW in title)'), '1' => t('Daily (use node containing match for YYYY-MM-DD in title)'), '2' => t('Node publish date (choose node to edit by creation time - ignores user timezone setting)')),
          '#description' => t('Drupal will use the user time zone to determine if there is a match unless you choose "Node publish date".'),
        );
        $form[$type]['node_tasklist_' . $type . '_frequency'] = array(
          '#type' => 'radios',
          '#title' => t('Node auto-creation frequency'),
          '#default_value' => variable_get('node_tasklist_' . $type . '_frequency', '0'),
          '#options' => array('none' => t('Do not generate nodes, I will find my own way to create them'), '0' => t('Weekly (create nodes named with YYYY-WW)'), '1' => t('Daily (create nodes named with YYYY-MM-DD)')),
          '#description' => t('Nodes are created during the first cron run on Monday, usually just after midnight, in the site time zone.'),
        );
        $form[$type]['node_tasklist_' . $type . '_multi'] = array(
          '#type' => 'checkbox',
          '#title' => t('Multiuser'),
          '#description' => t('Generates nodes for each user on the site with permission to edit this type (either edit own or edit any). Users must also have "access content" permission.'),
          '#default_value' => variable_get('node_tasklist_' . $type . '_multi', '0'),
          '#options' => array('0' => t('False'), '1' => t('True')),
          '#description' => 'Generates nodes for each user on the site with permission to edit this type (either edit own or edit any). Users must also have "access content" permission.  Nodes will appear to be authored by the user but the user does not need create permission for this setting to work.',
        );
      }
    }
  }
  return system_settings_form($form);
}

